import { getField, updateField } from 'vuex-map-fields'

const state = {
  country: '',
  firstName: '',
  lastName: '',
  organizationName: '',
  organizationPhone: '',
  email: '',
  password: ''
}

const mutations = {
  updateField
}

const getters = {
  getField
}

export default {
  state,
  mutations,
  getters
}