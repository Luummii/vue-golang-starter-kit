import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'

import '../semantic/dist/semantic.css'
import '../semantic/dist/semantic.js'

Vue.config.productionTip = false

new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
})
