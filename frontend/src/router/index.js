import Vue from 'vue'
import Router from 'vue-router'

import Events from '@/view/Events/Events'
import SignIn from '@/view/Auth/SignIn/SignIn'
import SignUp from '@/view/Auth/SignUp/SignUp'
import PageNoFound from '@/view/PageNoFound'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    { path: '/', alias: '/events',  name: 'Events', component: Events },
    { path: '/signin', name: 'SignIn', components: { auth: SignIn } },
    { path: '/signup', name: 'SignUp', components: { auth: SignUp } },
    { path: '/*', name: 'PageNoFound', component: PageNoFound }
  ]
})
